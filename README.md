### Żeby utworzyć pliki lohcnv z plików w formacjie vcf należy użyć polecenia: ###

```
#!bash

./run.sh /home/marpiech/seq-archive/analysis/cnv3/ 001-lohcnv.vcf,012-lohcnv.vcf,021-lohcnv.vcf,002-lohcnv.vcf,013-lohcnv.vcf /home/dzesikah/tmp

```
### gdzie kolejne argumenty to: ###

1. ścieżka do katalogu w którym znajdują się pliki vcf (/home/marpiech/seq-archive/analysis/cnv3/)
2. lista nazw plików vcf oddzielonych przecinkiem (001-lohcnv.vcf,012-lohcnv.vcf,021-lohcnv.vcf,002-lohcnv.vcf,013-lohcnv.vcf),
3. ścieżka do katalogu, w których chcemy mieć zapisane pliki lohcnv (/home/dzesikah/tmp)