import sys
import re
from re import match

for line in sys.stdin:
	wynik = ""

	lista = line.split("\t")
	
	if len(lista[3]) == 1 and len(lista[4]) == 1:
		info = lista[7].split(";")
	
		format = lista[8].split(":")
		format_value = lista[9].split(":")

		index_cov = (i for i,val in enumerate(format) if match('AD', val))
		index_cov = list(index_cov)
	
		if not index_cov:
			cov_ref = "0" + "\t"
			cov_alt = "0"

		else:
			index_cov = index_cov[0]
			cov = format_value[index_cov].split(",")

			cov_ref = cov[0] + "\t"
			cov_alt = cov[1]

		index_genotyp = (i for i,val in enumerate(format) if match('GT', val))
		index_genotyp = list(index_genotyp)[0]	

		genotyp_id = format_value[index_genotyp]

		if genotyp_id == "0/0":
			genotyp = "AA" + "\t"
		elif genotyp_id == "1/1":
			genotyp = "BB" + "\t"
		elif genotyp_id == "0/1" or genotyp_id == "1/0":
			genotyp = "AB" + "\t"
		else:
			genotyp = "AA" + "\t"


		wynik = wynik + lista[2] + "\t" + lista[0] + "\t" + lista[1] + "\t" + genotyp + cov_ref + cov_alt
		print wynik
