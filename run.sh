#!/bin/bash

rm -r /tmp/mk.lohcnv.file
mkdir /tmp/mk.lohcnv.file

echo "Pliki zostaną zapisane w katalogu: "$3

IFS=', ' read -r -a array <<< "$2"

for file in ${array[*]}
 do
  echo $file
  cat $1/$file | grep -v "#" | python script.py > /tmp/mk.lohcnv.file/$file
 done

./create.file.R $2 $3
